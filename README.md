# README

## docker-compose NHN

This repository holds the container setup for the websites of the former Nationaal Herbarium Nederland.

**Note of attention** on the branches used in this repository. There are 2 distinct branch that are needed for running a test server and a production server: `test` for the test server and `main` for the production server. Reason for this, are the URL's used by the websites. Production uses 3 different domains; the test server just one. This results in different traefik redirection rules for the containers. Please make sure you pay attention to that differince while modifying the setup and especially on merging changes!

URL's used by the test server:

- [https://nhn-tst-001.dryrun.link/](https://nhn-tst-001.dryrun.link/)
- [https://nhn-tst-001.dryrun.link/hermann/](https://nhn-tst-001.dryrun.link/hermann/)
- [https://nhn-tst-001.dryrun.link/lianas/](https://nhn-tst-001.dryrun.link/lianas/)


URL's used by the production server:

- [https://www.nationaalherbarium.nl/](https://www.nationaalherbarium.nl/)
- [https://www.hermann-herbarium.nl](https://www.hermann-herbarium.nl)
- [https://www.lianas-of-guyana.org](https://www.lianas-of-guyana.org)

## Precommit gitleaks

This project has been protected by [gitleaks](https://github.com/gitleaks/gitleaks).
The pipeline is configured to scan on leaked secrets.

To be sure you do not push any secrets,
please [follow our guidelines](https://docs.aob.naturalis.io/standards/secrets/),
install [precommit](https://pre-commit.com/#install)
and run the commands:

- `pre-commit autoupdate`
- `pre-commit install`
